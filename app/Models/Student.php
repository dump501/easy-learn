<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    
    public function enrolls()
    {
    	# code...
    	return $this->hasMany('App\Models\enroll');
    }
}
