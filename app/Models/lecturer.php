<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class lecturer extends Model
{
    use HasFactory;

    #return the admin that register the lecturer
    public function register_by_admin()
    {
    	# code...
    	return $this->belongsTo('App\Models\admin');
    }

    #return the courses teach by the lecturer
    public function teach_courses()
    {
    	# code...
    	return $this->hasMany('App\Models\course');
    }
}
