<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();


#admins
        DB::table('admins')->insert([
            'name' =>'admin',
            'email' => 'admin@gmail.com',
            'password' => 'admin',
        ]);

        DB::table('admins')->insert([
            'name' =>'admin2',
            'email' => 'admin2@gmail.com',
            'password' => 'admin2',
        ]);


#courses
        DB::table('courses')->insert([
            'name' =>'java programming',
            'code'=>'CSCT 4108',
            'id_lecturer'=>'1',
            'id_admin'=>'1',
        ]);

        DB::table('courses')->insert([
            'name' =>'Python programming',
            'code'=>'CSCT 4109',
            'id_lecturer'=>'2',
            'id_admin'=>'1',
        ]);

        DB::table('courses')->insert([
            'name' =>'C++ programming',
            'code'=>'CSCT 4102',
            'id_lecturer'=>'2',
            'id_admin'=>'1',
        ]);
        DB::table('courses')->insert([
            'name' =>'C programming',
            'code'=>'CSCT 4101',
            'id_lecturer'=>'1',
            'id_admin'=>'1',
        ]);


#enroll
        DB::table('enrolls')->insert([
            'id_course' =>'1',
            'matricule'=>'Uba18T0415',
            'academic_year'=>'2018/2019',
            
        ]);
        DB::table('enrolls')->insert([
            'id_course' =>'2',
            'matricule'=>'Uba18T0415',
            'academic_year'=>'2018/2019',
            
        ]);
        DB::table('enrolls')->insert([
            'id_course' =>'1',
            'matricule'=>'Uba18T0416',
            'academic_year'=>'2018/2019',
            
        ]);
        DB::table('enrolls')->insert([
            'id_course' =>'2',
            'matricule'=>'Uba18T0416',
            'academic_year'=>'2018/2019',
            
        ]);

#student
        DB::table('Students')->insert([
        	'matricule'=>'Uba18T0415',
            'name'=>'Meli',
            'surname'=>'archille',
            'password'=>'Meli',
        ]);

        DB::table('Students')->insert([
        	'matricule'=>'Uba18T0416',
            'name'=>'Tsafack',
            'surname'=>'archille',
            'password'=>'tsafack',
        ]);
#lecturer
        DB::table('lecturers')->insert([
        	'email' => 'meli@gmail.com',
            'name'=>'Meli L',
            
            'password'=>'Meli',
        ]);



    }
}
