@extends('layouts.master')

@section('topbar')
<nav class="navbar navbar-expand navbar-light bg-primary topbar mb-4 static-top shadow">
    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a href="#" class="nav-link">Home</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Contact developpers</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">about developpers</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" data-toggle="modal" data-target="#largeModal"> Login</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">register</a>
      </li>
    </ul>
</nav>    
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="max-height: 400px">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" style="max-height: 400px">
                      <div class="carousel-item active">
                        <img src="{{ asset('images/bg-title-01.jpg') }}" class="d-block w-100" alt="...">
                      </div>
                      <div class="carousel-item">
                        <img src="{{ asset('images/bg-title-02.jpg') }}" class="d-block w-100" alt="...">
                      </div>
                      <div class="carousel-item">
                        <img src="{{ asset('images/work3.jpg') }}" class="d-block w-100" alt="...">
                      </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row pt-3">
            <div class="col-12 col-md-4">
                <center><i class="fa fa-users fa-5x text-primary"></i><br>
                    <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                        Exercitationem maxime saepe quis rerum optio.
                    </p>
                </center> 
            </div>
            <div class="col-12 col-md-4">
                <center><i class="fa fa-users fa-5x text-primary"></i><br>
                    <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                        Exercitationem maxime saepe quis rerum optio.
                    </p>
                </center>
            </div>
            <div class="col-12 col-md-4">
                <center><i class="fa fa-users fa-5x text-primary"></i><br>
                    <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                        Exercitationem maxime saepe quis rerum optio.
                    </p>
                </center>
            </div>
        </div>
    </div>

      <!-- login modal -->

      <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Large Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <a href="{{ route('login') }}">
                                <div class="card login-card">
                                    <div class="card-body">
                                        <center><i class="fas fa-user fa-5x"></i></center>
                                    </div>
                                    <div class="card-footer">
                                        <center>Login as admin</center>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{ route('login') }}">
                                <div class="card login-card">
                                    <div class="card-body">
                                        <center><i class="fas fa-user fa-5x"></i></center>
                                    </div>
                                    <div class="card-footer">
                                        <center>Login as Lecturer</center>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{ route('login') }}">
                                <div class="card login-card">
                                    <div class="card-body">
                                        <center><i class="fas fa-user fa-5x"></i></center>
                                    </div>
                                    <div class="card-footer">
                                        <center>Login as Student</center>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
  <!-- end login modal -->
@endsection

@section('script')
    <script>
        /*$('.login-card').hover(function(){
            $('.login-card').addClass('rotate-15');
        }, function(){
            $('.login-card').removeClass('rotate-15');
        });*/
    </script>
@endsection